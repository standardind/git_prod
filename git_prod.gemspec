# frozen_string_literal: true

require_relative "lib/git_prod/version"

Gem::Specification.new do |spec|
  spec.name        = "git_prod"
  spec.version     = GitProd::VERSION
  spec.authors     = ["Ryan Kingston"]
  spec.email       = ["ryanmk54@gmail.com"]
  spec.homepage    = "https://bitbucket.org/standardind/git_prod/src/master/"
  spec.summary     = "Git productivity statistics"
  spec.description = "Git productivity statistics"
  spec.license     = "MIT"
  spec.required_ruby_version = ">= 2.6.3"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/standardind/git_prod/src/master/"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/standardind/git_prod/src/master/CHANGELOG.md"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "query_filter", "~> 0.2.2"
  spec.add_dependency "rails", ">= 4.0", "< 6.1"
  spec.add_development_dependency "rubocop-rails_config", "~> 1.6"
end
