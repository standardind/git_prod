# frozen_string_literal: true

class CreateGitProdDailyStatistics < ActiveRecord::Migration[6.0]
  def change
    create_table :git_prod_daily_statistics do |t|
      t.date :commit_date, null: false
      t.string :author_name, null: false
      t.string :author_email, null: false
      t.string :repository_name, null: false
      t.integer :num_commits, null: false
      t.integer :num_insertions, null: false
      t.integer :num_deletions, null: false

      t.timestamps

      # The indices below have been given names because
      # the Rails default is longer than the default of 63 characters

      # The Developer statistics widget allows filtering by commit_date
      t.index(:commit_date, name: "idx_git_prod_daily_stats_commit_date")
      t.index([:repository_name, :commit_date, :author_email], unique: true, name: "idx_git_prod_daily_stats_repo_name_commit_date_author_email")
    end
  end
end
