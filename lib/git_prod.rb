# frozen_string_literal: true

require "git_prod/version"
require "git_prod/engine"

module GitProd
  class Error < StandardError; end

  def self.config
    GitProd::Configuration.instance
  end

  def self.default_commit_date_from
    1.day.ago.in_time_zone(config.time_zone).beginning_of_day
  end

  def self.default_commit_date_to
    1.day.ago.in_time_zone(config.time_zone).end_of_day
  end

  def self.logger
    @logger || Rails.logger
  end

  # @param logdev The log device. This is a filename (String) or IO object (typically STDOUT, STDERR, or an open file).
  def self.logger=(logdev)
    @logger = Logger.new(logdev)
  end

  def self.repository_names
    Dir.children(config.clone_path)
  end

  def self.update_repositories
    if config.repository_urls.nil?
      raise GitProd::Error.new("repository_urls need to be set in config/git_prod.yml")
    end

    config.repository_urls.each do |repository_url|
      repository_name = GitProd::Repository.name_from_url(repository_url)
      repository_path = GitProd::Repository.repository_path(repository_name)

      # If the repo already exists, update it, otherwise, clone it
      if Dir.exist?(repository_path)
        GitProd::Repository.new(repository_name).fetch
      else
        GitProd::Repository.clone(repository_url)
      end
    end
  end

  def self.remove_repositories
    spawn("rm -rf ./*", chdir: GitProd.config.clone_path)
  end

  # @param date {Time}
  # use America/Denver time for the default b/c that is the time zone we work in
  def self.aggregate_commits(date_range)
    commits = Dir.children(config.clone_path).reduce([]) do |memo, repository_name|
      memo + GitProd::Repository.new(repository_name).commits(date_range)
    end
    GitProd::DailyStat.attributes_from_commits(commits)
  end

  def self.verify_command(cmd, output, status)
    # 0 is the Unix status for success
    return if status == 0

    raise GitProd::Error.new(
      "Command failed with status #{status}\n" +
      cmd + "\n" + output
    )
  end

  # @param status_in {Array}
  def self.verify_command_status_in(cmd, output, status, status_in)
    return if status_in.include?(status.exitstatus)

    error = "Command failed with status #{status.exitstatus}\n#{cmd}\n#{output}"
    raise GitProd::Error.new(error)
  end
end

require_relative "./git_prod/configuration"
require_relative "./git_prod/daily_statistic_builder"
require_relative "./git_prod/repository"
