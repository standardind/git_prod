# frozen_string_literal: true

module GitProd::DailyStatisticBuilder
  def self.build(repository_name, commit_date_from = nil)
    date_range = commit_date_from..GitProd.default_commit_date_to
    repository = GitProd::Repository.new(repository_name)
    repository.commits(date_range)
      .group_by do |commit|
        {
          author_email: commit.author_email,
          author_name: commit.author_name,
          commit_date: commit.commit_date
        }
      end
      .transform_values do |commits|
        {
          num_commits: commits.length,
          num_insertions: commits.sum(&:num_insertions),
          num_deletions: commits.sum(&:num_deletions)
        }
      end
      .map do |key, value|
        {
          **key, **value,
          repository_name: repository_name,
          created_at: Time.current,
          updated_at: Time.current
        }
      end
  end
end
