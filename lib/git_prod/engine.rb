# frozen_string_literal: true

module GitProd
  class Engine < ::Rails::Engine
    isolate_namespace GitProd
    config.generators.api_only = true
  end
end
