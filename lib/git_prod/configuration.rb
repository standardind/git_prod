# frozen_string_literal: true

class GitProd::Configuration
  include Singleton

  def root_directory
    Dir.pwd
  end

  private
    def initialize
      @config = Psych.load_file("config/git_prod.yml")[Rails.env].freeze
    end

    def method_missing(method, *args)
      @config[method.to_s]
    end
end
