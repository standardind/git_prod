# frozen_string_literal: true

require "open3"

class GitProd::Repository
  Struct.new(
    "Commit",
    :author_name, :author_email, :commit_date, :num_insertions, :num_deletions
  )

  def self.name_from_url(url)
    extname = File.extname(url)
    File.basename(url, extname)
  end

  def self.clone(url)
    system("mkdir -p #{GitProd.config.clone_path}", exception: true)
    repository_name = name_from_url(url)
    GitProd.logger.info("Cloning #{repository_name}")
    cmd = "git clone --mirror #{url} #{repository_name} #{shallow_since}"
    output, status, = Open3.capture2e(cmd, chdir: GitProd.config.clone_path)
    GitProd.verify_command_status_in(cmd, output, status, [0, 128])
  end

  def set_mailmap_file
    relative_mailmap_path = Pathname
      .new(GitProd.config.mailmap_path)
      .relative_path_from(repository_path)
    cmd = <<~STR
      cd #{@repository_name} &&
      git config mailmap.file #{relative_mailmap_path};
    STR
    output, status, = Open3.capture2e(cmd, chdir: GitProd.config.clone_path)
    GitProd.verify_command_status_in(cmd, output, status, [0, 128])
  end

  def self.repository_path(repository_name)
    File.join(GitProd.config.clone_path, repository_name)
  end

  def self.shallow_since
    GitProd.config.shallow_since.blank? ?
      "" : %Q[--shallow-since="#{GitProd.config.shallow_since}"]
  end

  def initialize(repository_name)
    @repository_name = repository_name
  end

  def available_date_range
    # a is for author
    # c is for committer
    # i is for ISO8601

    cmd = 'git log --max-parents=0 --format="%ai"'
    oldest_commit_date, status = Open3.capture2e(cmd, chdir: repository_path)
    GitProd.verify_command(cmd, oldest_commit_date, status)

    cmd = 'git log -1 --format="%ci"'
    latest_commit_date, status = Open3.capture2e(cmd, chdir: repository_path)
    GitProd.verify_command(cmd, latest_commit_date, status)

    Time.parse(oldest_commit_date)..Time.parse(latest_commit_date)
  end

  def last_fetch_attempt
    # The date of FETCH_HEAD will be updated, even if the fetch failed
    fetch_head_path = File.join(repository_path, "FETCH_HEAD")
    head_path = File.join(repository_path, "HEAD")
    file_to_check = File.exist?(fetch_head_path) ? fetch_head_path : head_path
    File.mtime(file_to_check)
  end

  def fetch
    # Instead of doing a shallow commit, I'll fetch at most once per day
    # cmd = %Q[git fetch --shallow-since="#{required_date_range.begin}"]
    cmd = "git fetch #{self.class.shallow_since}"
    GitProd.logger.info("Fetching #{@repository_name}")
    output, status = Open3.capture2e(cmd, chdir: repository_path)
    GitProd.verify_command_status_in(cmd, output, status, [0, 128])
  end

  # @param date {Range} date range to get commits for
  def commits(date_range)
    set_mailmap_file

    # %aN author name
    # %n newline
    # %aE author email
    # %ci commit ISO 8601 date
    # --branches gets commits for all branches
    cmd = %Q[git log --branches --before="#{date_range.end}" --after="#{date_range.begin}" --no-merges --format="%aN%n%aE%n%ci" --shortstat]
    GitProd.logger.info("Getting commits for #{@repository_name}")
    GitProd.logger.debug(cmd)
    output, _status = Open3.capture2(cmd, chdir: repository_path)

    commits = []
    output_enum = output.lines.map { |line| line.strip }.each
    loop do
      author_name = output_enum.next
      author_email = output_enum.next
      commit_date = output_enum.next.split(" ")[0]

      # I don't know how, but somehow there was a bunch of commits that say
      # "Merged benjibranch into master"
      # I used the flag --no-merges and they are still there,
      # which means they must have one parent
      # These commits don't have any shortstat info
      # asociated with them and are of no value
      next unless output_enum.peek.empty?

      output_enum.next
      shortstat = output_enum.next
      num_insertions = /(\d+) insertion/.match(shortstat) { |m| m[1].to_i } || 0
      num_deletions = /(\d+) deletion/.match(shortstat) { |m| m[1].to_i } || 0
      commits.push(Struct::Commit.new(
        author_name, author_email, commit_date, num_insertions, num_deletions
      ))
    end
    commits
  end

  def repository_path
    self.class.repository_path(@repository_name)
  end

  # def num_commits
  #   cmd = <<~STR
  #     git shortlog --all -s --since="1 week ago" --email
  #   STR
  #   output, _status = Open3.capture2(cmd, chdir: @repository_path)
  #   STDOUT << output
  #   output.split("\n").map { |row| row.split("\t") }
  # end
end
