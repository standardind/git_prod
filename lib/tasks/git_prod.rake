# frozen_string_literal: true

namespace :git_prod do
  desc "Install migrations and ssh keys"
  task install: %w[install:migrations install:ssh_keys]

  desc "Update repositories and statistics"
  task update: %w[repositories:update statistics:update]
end
