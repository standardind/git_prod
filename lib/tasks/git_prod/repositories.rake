# frozen_string_literal: true

namespace "git_prod:repositories" do
  desc "Remove all repositories that have been cloned by GitProd"
  task remove: [:environment] do |task|
    GitProd.logger = STDOUT
    GitProd.remove_repositories
  end

  desc "Update all repositories defined in config/git_prod.yml"
  task update: [:environment] do |task|
    GitProd.logger = STDOUT
    GitProd.update_repositories
  end
end
