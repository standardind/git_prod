# frozen_string_literal: true

def add_ssh_key
  added_ssh_key = system <<~CMD
    ssh-keygen -C "git_prod" -f ~/.ssh/git_prod -N "" &&
  CMD
  unless added_ssh_key
    STDERR.puts("Unable to add ssh key")
    abort
  end

  puts <<~STR

  # First #
  Open ~/.ssh/git_prod.pub and copy its contents.

  # From Bitbucket #
  1. Choose *Personal settings* from your avatar in the lower left.
  2. Click *SSH keys*. If you've already added keys, you'll see them on this page.
  3. From Bitbucket, click *Add key*.
  4. Enter a *Label* for your new key, for example, Default public key.
  5. Paste the copied public key into the SSH *Key* field.
     If you manually copied the key, make sure you copy the entire key,
     which starts with ssh-ed25519 or ssh-rsa, and ends with git_prod.
  6. Click *Save*.
     Bitbucket sends you an email to confirm the addition of the key.
  7. Return to the command line and verify your configuration and
     username by entering the following command:
     $ ssh -T git@bitbucket.org -i ~/.ssh/git_prod

  # From Git
  1. In the top right corner, select your avatar.
  2. Select Preferences.
  3. From the left sidebar, select SSH Keys.
  4. In the Key box, paste the contents of your public key.
     If you manually copied the key, make sure you copy the entire key,
     which starts with ssh-ed25519 or ssh-rsa, and ends with git_prod.
  5. In the Title text box, type a description, like Work Laptop or
     Home Workstation.
  6.  Select Add key.
  7. Return to the command line and verify your configuration and
     username by entering the following command:
     $ ssh -T git@git.owendistributing.net -i ~/.ssh/git_prod
  STR
end

namespace "git_prod:install" do
  desc "Add ssh keys"
  task :ssh_keys do |task|
    add_ssh_key unless File.exist?(File.expand_path("~/.ssh/git_prod"))

    File.open(File.expand_path("~/.bashrc"), "a") do |file|
      # Add ssh-agent to .bashrc unless it's already running
      file.puts("eval `ssh-agent`") unless ENV.key?("SSH_AGENT_PID")

      # Add git_prod to ssh-agent unless it's already been added
      stdout_str, _status = Open3.capture2("ssh-add -l")
      if stdout_str.include? "git_prod"
        puts "git_prod ssh key already exists"
      else
        file.puts("ssh-add ~/.ssh/git_prod")

        puts "Run command below to enable new ssh keys"
        puts ". ~/.bashrc"
      end
    end
  end
end
