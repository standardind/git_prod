# frozen_string_literal: true

namespace "git_prod:statistics" do
  desc "Update statistics"
  task update: [:environment] do |task, args|
    GitProd.logger = STDOUT

    GitProd.repository_names.each do |repository_name|
      commit_date_from = ENV["COMMIT_DATE_FROM"] ?
        Date.parse(ENV["COMMIT_DATE_FROM"]) : (
          GitProd::DailyStatistic
            .where(repository_name: repository_name)
            .maximum(:commit_date) ||
          GitProd.default_commit_date_from
        )

      daily_statistics_attributes = GitProd::DailyStatisticBuilder.build(
        repository_name, commit_date_from
      )
      next if daily_statistics_attributes.empty?

      # Delete any statistics that may conflict
      # If the email address was changed and this is run
      # again, there may be duplicate data
      GitProd::DailyStatistic
        .where(repository_name: repository_name)
        .where("commit_date >= ?", commit_date_from)
        .delete_all

      GitProd::DailyStatistic.upsert_all(
        daily_statistics_attributes,
        unique_by: %i[repository_name commit_date author_email]
      )
    end
  end
end
