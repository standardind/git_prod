# frozen_string_literal: true

class GitProd::DailyStatisticFilter < QueryFilter::Base
  date_range :commit_date

  # { commit_date: '06/24/2017 to 06/30/2017' }
  def date_range_commit_date(period)
    query.where(commit_date: period.range)
  end
end
