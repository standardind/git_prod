# frozen_string_literal: true

module GitProd
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true

    connects_to database: { writing: :sbm, reading: :sbm }
  end
end
