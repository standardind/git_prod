# frozen_string_literal: true

class GitProd::DailyStatistic < GitProd::ApplicationRecord
  def self.available_date_range
    connection.exec_query(
      "SELECT MIN(commit_date), MAX(commit_date) FROM git_prod_daily_statistics"
    )[0]
  end

  def self.filter(params)
    GitProd::DailyStatisticFilter.new(all, params).to_query
  end
end
