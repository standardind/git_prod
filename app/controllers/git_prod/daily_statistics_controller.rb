# frozen_string_literal: true

class GitProd::DailyStatisticsController < ApplicationController
  def available_date_range
    render json: GitProd::DailyStatistic.available_date_range
  end

  def index
    statistics = GitProd::DailyStatistic.filter(params[:filter])
    render json: statistics, except: %i[created_at updated_at]
  end
end
