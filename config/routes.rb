# frozen_string_literal: true

GitProd::Engine.routes.draw do
  resources :daily_statistics, only: :index do
    get :available_date_range, on: :collection
  end
end
